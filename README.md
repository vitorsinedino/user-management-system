<div>
  <h1 align="center">
    <br>
    <a><img src="https://i.ibb.co/ZMyPcGq/Accept-tasks-rafiki.png" alt="User Management" width="200"></a>
    <br>
    <br>
  </h1>
</div>

<div>
  <h4 align="center">User Management Platform Build on Top Of  <a href="https://developer.mozilla.org/pt-BR/docs/Web/JavaScript" target="_blank">Javascript</a>.</h4>


  <p align="center">
      <a href="#key-features">Key Features</a> •
      <a href="#how-to-use">How To Use</a> •
      <a href="#license">License</a>
  </p>

  <p align="center" style="font-size:32px">
      Home
  </p>
</div>

<div align="center">
--------------------------- ![screenshot](https://i.ibb.co/8dDwGP4/Fire-Shot-Capture-011-User-Sanagement-System-localhost.png)---------------------------
</div>
<br>
<br>


## Key Features

* Creation of users
* Simple design using bootstrap
* Fully responsive and mobile fryendly 

## How To Use

To clone and run this application, you'll need [Git](https://git-scm.com), NodeJS (https://nodejs.org/en/) and [Xampp(PHP,Apache,Mysql)],(https://www.docker.com/get-started))  installed on your computer.

Note 1: before doing these steps make sure to have Apache and Mysql are running
Note 2: before doing these steps make sure to have creatade a database folowing the schema file inside the root folder of the project
Note 3: Make sure you have configured the env file according to your db settings

<b>From your command line:</b>

```bash
# Clone this repository
$ git clone https://gitlab.com/vitorsinedino/user-management-system.git

# Go into the repository
$ cd user-management-system

# Run npm install
$ npm install

# Wait for the whole thing to stop moving

# open your browser and use as url:
https://localhost:(the port you seted on env file)

```


## License

MIT

---


