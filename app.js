const express = require('express');
const exphbs = require('express-handlebars');
const bodyParser = require('body-parser');
const mysql = require('mysql');

require('dotenv').config();

const app = express();
const port = process.env.PORT || 5000;

//parsing middleware
// parse aplication
app.use(bodyParser.urlencoded({extended: false}));

//parse app/json
app.use(bodyParser.json());


//static files
app.use(express.static('public'))

//templating engine
app.engine('hbs', exphbs({extname:'.hbs'}));
app.set('view engine', 'hbs');


//router
const routes = require('./server/routes/user');
app.use('/', routes);

app.listen(port, () => console.log(`listening on port ${port}`));